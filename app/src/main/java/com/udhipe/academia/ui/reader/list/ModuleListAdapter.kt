package com.udhipe.academia.ui.reader.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.udhipe.academia.data.ModuleEntity
import com.udhipe.academia.databinding.ItemsModuleListCustomBinding

class ModuleListAdapter internal constructor(private val listener: MyAdapterClickListener) :
    RecyclerView.Adapter<ModuleListAdapter.ModuleViewHolder>() {
    private var moduleList = ArrayList<ModuleEntity>()

    internal interface MyAdapterClickListener {
        fun onItemClicked(position: Int, moduleId: String)
    }

    internal fun setModule(moduleList: List<ModuleEntity>?) {
        if (moduleList == null) return
        this.moduleList.clear()
        this.moduleList.addAll(moduleList)
    }

    class ModuleViewHolder(private val binding: ItemsModuleListCustomBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(module: ModuleEntity) {
            binding.textModuleTitle.text = module.title
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ModuleListAdapter.ModuleViewHolder {
        val binding =
            ItemsModuleListCustomBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ModuleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ModuleListAdapter.ModuleViewHolder, position: Int) {
        val module = moduleList[position]
        holder.bind(module)
        holder.itemView.setOnClickListener {
            listener.onItemClicked(
                holder.adapterPosition,
                moduleList[holder.adapterPosition].moduleId
            )
        }
    }

    override fun getItemCount(): Int {
        return moduleList.size
    }
}