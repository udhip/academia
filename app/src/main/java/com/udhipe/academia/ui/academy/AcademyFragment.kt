package com.udhipe.academia.ui.academy

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.udhipe.academia.R
import com.udhipe.academia.databinding.FragmentAcademyBinding
import com.udhipe.academia.util.DataDummy

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class AcademyFragment : Fragment() {

    private var fragmentAcademyBinding: FragmentAcademyBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = fragmentAcademyBinding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        fragmentAcademyBinding = FragmentAcademyBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        binding.buttonFirst.setOnClickListener {
//            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
//        }

        if (activity != null) {
            // yang peertama pakai data langsung dari datadummy
//            val courses = DataDummy.generateDummyCourses()

            // yang kedua minta data dari videmodel, viewmodel dari datadummy
            // ini model sintaks apa ya?
            val academyViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())[AcademyViewModel::class.java]
            val courses = academyViewModel.getCourses()

            val academyAdapter = AcademyAdapter()
            academyAdapter.setCourses(courses)

            fragmentAcademyBinding?.let {
                with(it.rvAcademy) {
                    layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    setHasFixedSize(true)
                    adapter = academyAdapter
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentAcademyBinding = null
    }
}