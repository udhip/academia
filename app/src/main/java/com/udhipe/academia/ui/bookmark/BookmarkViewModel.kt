package com.udhipe.academia.ui.bookmark

import androidx.lifecycle.ViewModel
import com.udhipe.academia.data.CourseEntity
import com.udhipe.academia.util.DataDummy

class BookmarkViewModel : ViewModel() {
    fun getCourses(): List<CourseEntity> = DataDummy.generateDummyCourses()
}