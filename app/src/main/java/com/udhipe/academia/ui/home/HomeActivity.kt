package com.udhipe.academia.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.udhipe.academia.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val activityHomeBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(activityHomeBinding.root)

        val sectionsPagerAdaper = SectionsPagerAdaper(this, supportFragmentManager)
        activityHomeBinding.viewPager.adapter = sectionsPagerAdaper
        activityHomeBinding.tabs.setupWithViewPager(activityHomeBinding.viewPager)

        supportActionBar?.elevation = 0f
    }
}