package com.udhipe.academia.ui.detail

import android.content.Intent
import android.os.Bundle
import android.view.RoundedCorner
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.udhipe.academia.R
import com.udhipe.academia.data.CourseEntity
import com.udhipe.academia.databinding.ActivityDetailCourseBinding
import com.udhipe.academia.databinding.ContentDetailCourseBinding
import com.udhipe.academia.ui.reader.CourseReaderActivity
import com.udhipe.academia.util.DataDummy

class DetailCourseActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_COURSE = "extra_course"
    }

    private lateinit var appBarConfiguration: AppBarConfiguration
//    private lateinit var detailCourseBinding: ContentDetailCourseBinding

    private lateinit var contentDetailCourseBinding: ContentDetailCourseBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val activityDetailCourseBinding = ActivityDetailCourseBinding.inflate(layoutInflater)
        contentDetailCourseBinding = activityDetailCourseBinding.detailContent
        setContentView(activityDetailCourseBinding.root)

        setSupportActionBar(activityDetailCourseBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        activityDetailCourseBinding.toolbar.setNavigationOnClickListener {
            finish()
        }

        val adapter = DetailCourseAdapter()

        val detailCourseViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        )[DetailCourseViewModel::class.java]

        val extras = intent.extras
        if (extras != null) {
            val courseId = extras.getString(EXTRA_COURSE)
            if (courseId != null) {
                // first step using this
                //   val modules = DataDummy.generateDummyModules(courseId)

                // second step
                detailCourseViewModel.setSelectedCourse(courseId)
                val modules = detailCourseViewModel.getModules()
                adapter.setModules(modules)

                // first step
//                for (course in DataDummy.generateDummyCourses()) {
//                    if (course.courseId == courseId) {
//                        populateCourse(course)
//                    }
//                }

                // sedond step
                val course = detailCourseViewModel.getCourse()
                populateCourse(course)
            }
        }

        with(contentDetailCourseBinding.rvModule) {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(this@DetailCourseActivity)
            setHasFixedSize(true)
            this.adapter = adapter
            val dividerItemDecoration =
                DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL)
            addItemDecoration(dividerItemDecoration)
        }

//        val navController = findNavController(R.id.nav_host_fragment_content_detail_course)
//        appBarConfiguration = AppBarConfiguration(navController.graph)
//        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    private fun populateCourse(course: CourseEntity) {
        contentDetailCourseBinding.textTitle.text = course.title
        contentDetailCourseBinding.textDescription.text = course.description
        contentDetailCourseBinding.textDate.text =
            resources.getString(R.string.deadline_date, course.deadline)

        Glide.with(this)
            .load(course.imagePath)
            .transform(RoundedCorners(20))
            .apply(RequestOptions.placeholderOf(R.drawable.ic_loading))
            .error(R.drawable.ic_error)
            .into(contentDetailCourseBinding.imagePoster)

        contentDetailCourseBinding.btnStart.setOnClickListener {
            val intent = Intent(this@DetailCourseActivity, CourseReaderActivity::class.java)
            intent.putExtra(CourseReaderActivity.EXTRA_COURSE_ID, course.courseId)
            startActivity(intent)
        }
    }

//    override fun onSupportNavigateUp(): Boolean {
//        val navController = findNavController(R.id.nav_host_fragment_content_detail_course)
//        return navController.navigateUp(appBarConfiguration)
//                || super.onSupportNavigateUp()
//    }
}