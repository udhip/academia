package com.udhipe.academia.ui.bookmark

import com.udhipe.academia.data.CourseEntity

interface BookmarkFragmentCallback {
    fun onShareClick(courseEntity: CourseEntity)
}
