package com.udhipe.academia.ui.academy

import androidx.lifecycle.ViewModel
import com.udhipe.academia.data.CourseEntity
import com.udhipe.academia.util.DataDummy

class AcademyViewModel : ViewModel() {
    fun getCourses(): List<CourseEntity> = DataDummy.generateDummyCourses()
}