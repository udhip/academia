package com.udhipe.academia.ui.reader

interface CourseReaderCallback {
    fun moveTo(position: Int, moduleId: String)
}