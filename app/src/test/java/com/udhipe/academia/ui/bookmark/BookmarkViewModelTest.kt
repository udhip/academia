package com.udhipe.academia.ui.bookmark

import org.junit.Assert.*
import org.junit.Before

import org.junit.Test

class BookmarkViewModelTest {

    lateinit var bookmarkViewModel: BookmarkViewModel

    @Before
    fun setup() {
        bookmarkViewModel = BookmarkViewModel()
    }

    @Test
    fun getCourses() {
        val courses = bookmarkViewModel.getCourses()
        assertNotNull(courses)
        assertEquals(5, courses.size)
    }
}