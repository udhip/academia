package com.udhipe.academia.ui.academy

import androidx.lifecycle.ViewModelProvider
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test

class AcademyViewModelTest {

    lateinit var academyViewModel: AcademyViewModel

    @Before
    fun setup(){
        academyViewModel = AcademyViewModel()
    }

    @Test
    fun getCourses() {
        val courses = academyViewModel.getCourses()
        assertNotNull(courses)
        assertEquals(5, courses.size)
    }
}