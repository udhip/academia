package com.udhipe.academia.ui.detail

import com.udhipe.academia.util.DataDummy
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test

class DetailCourseViewModelTest {

    private lateinit var detailCourseViewModel: DetailCourseViewModel
    private val dummyCourse = DataDummy.generateDummyCourses()[0]
    private val courseId = dummyCourse.courseId

    @Before
    fun setup() {
        detailCourseViewModel = DetailCourseViewModel()
        detailCourseViewModel.setSelectedCourse(courseId)
    }

    // tested when setup setselectedcourse and getModules
    @Test
    fun setSelectedCourse() {
    }

    @Test
    fun getCourse() {
        val courses = detailCourseViewModel.getCourse()
        assertNotNull(courses)
        assertEquals(dummyCourse.imagePath, courses.imagePath)
        assertEquals(dummyCourse.deadline, courses.deadline)
        assertEquals(dummyCourse.title, courses.title)
        assertEquals(dummyCourse.bookmarked, courses.bookmarked)
        assertEquals(dummyCourse.courseId, courses.courseId)
        assertEquals(dummyCourse.description, courses.description)
    }

    @Test
    fun getModules() {
        val modules = detailCourseViewModel.getModules()
        assertNotNull(modules)
        assertEquals(7, modules.size)
    }
}