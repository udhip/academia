package com.udhipe.academia.ui.reader

import com.udhipe.academia.data.ContentEntity
import com.udhipe.academia.util.DataDummy
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CourseReaderViewModelTest {

    private lateinit var courseReaderViewModel: CourseReaderViewModel
    private val dummyCourse = DataDummy.generateDummyCourses()[0]
    private val dummyCourseId = dummyCourse.courseId
    private val dummyModule = DataDummy.generateDummyModules(dummyCourseId)[0]
    private val dummyModuleId = dummyModule.moduleId

    @Before
    fun setUp() {
        courseReaderViewModel = CourseReaderViewModel()
        courseReaderViewModel.setSelectedCourseId(dummyCourseId)
        courseReaderViewModel.setSelectedModuleId(dummyModuleId)

        dummyModule.contentEntity = ContentEntity("<h3 class=\\\\\\\"fr-text-bordered\\\\\\\">Contoh Content</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>")
    }

    // tested in getModules
    @Test
    fun setSelectedCourseId() {
    }

    // tested in getSelectedModule
    @Test
    fun setSelectedModuleId() {
    }

    @Test
    fun getModules() {
        val modules = courseReaderViewModel.getModules()
        assertNotNull(modules)
        assertEquals(7, modules.size)
    }

    @Test
    fun getSelectedModule() {
        val selectedModule = courseReaderViewModel.getSelectedModule()
        assertNotNull(selectedModule)
        val contentEntity = selectedModule.contentEntity
        assertNotNull(contentEntity)
        val content = contentEntity?.content
        assertNotNull(content)
        assertEquals(dummyModule.contentEntity?.content, content)
        assertEquals(dummyModule.contentEntity, selectedModule.contentEntity)
        assertEquals(dummyModule.courseId, selectedModule.courseId)
        assertEquals(dummyModule.moduleId, selectedModule.moduleId)
        assertEquals(dummyModule.position, selectedModule.position)
        assertEquals(dummyModule.read, selectedModule.read)
        assertEquals(dummyModule.title, selectedModule.title)
    }
}